<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('price', 7, 2);
            $table->string('name', 50);
            $table->string('sku', 15);
            $table->string('description');
            $table->decimal('qty', 5, 2);
            $table->decimal('height', 5, 2);
            $table->decimal('width', 5, 2);
            $table->decimal('length', 5, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
