<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;

class Contact extends Controller
{
    public function show()
    {
    	return view('contact');
    }

    public function sendMessage(Request $request)
    {
        $name = $request->input('firstname', '');
        $lastname = $request->input('lastname', '');
        $phone = $request->input('phone', '');
        $email = $request->input('email', '');
        $message = $request->input('message', '');
        if ($name == '' || $lastname == '' || $phone == '' || $email == '' || $message == '') {
            return redirect()->route('contact')->withInput();
        }
        $contact = new App\Contact;
        $contact->name = $name;
        $contact->last_name = $lastname;
        $contact->phone = $phone;
        $contact->email = $email;
        $contact->body = $message;
        $contact->save();
        return redirect()->route('contact');
    }
}
