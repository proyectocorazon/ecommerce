<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App;
use View;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function __construct() {
        $categories = [];
        $model = App\Category::all();
        $link = ['', '', '', '', ''];
    	$actual_link = $_SERVER['REQUEST_URI'];
    	$key = strpos($actual_link, '/ecommerce/public/categoria/');
    	$key2 = strpos($actual_link, '/ecommerce/public/producto/');

    	if($actual_link == '/ecommerce/public/inicio'){
    		$link[0] = 'active';
    	}

    	if(($key === 0) || ($key2 === 0)){
    		$link[1] = 'active';
    	}

    	if($actual_link == '/ecommerce/public/nosotros'){
    		$link[2] = 'active';
    	}

    	if($actual_link == '/ecommerce/public/tutorial'){
    		$link[3] = 'active';
    	}

    	if($actual_link == '/ecommerce/public/contacto'){
    		$link[4] = 'active';
    	}

        foreach ($model as $category) {
            $categories[$category->id] = [
                'id' => $category->id,
                'name' => $category->name,
            ];
        }
        View::share('dropcategories', $categories);
        View::share('link', $link);
    }

}
