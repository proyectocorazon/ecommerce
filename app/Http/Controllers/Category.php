<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;

class Category extends Controller
{
    /*
     * Mostrar la pagina de la categoria id
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $category = App\Category::find($id);
        if (is_null($category)) {
            return redirect('categoria');
        }
        $productos = [];
        foreach ($category->products as $producto) {
            $image = '';
            foreach ($producto->images as $img) {
                $image = $img->url;
            }
            $productos[$producto->id] = [
                'name' => $producto->name, 
                'image' => $image,
                'id' => $producto->id,
                'price' => number_format($producto->price, 2)
            ];
        }
        $modelo = App\Category::all();
        $categorias = [];
        foreach ($modelo as $categoria) {
            if (is_null($categoria->parent_id)){
                $categorias[$categoria->id] = [
                    'id' => $categoria->id,
                    'name' => $categoria->name,
                    'products' => $categoria->products()->count(),
                    'categories' => []
                ];
            } else {
                $categorias[$categoria->parent_id]['categories'][] = [
                    'id' => $categoria->id,
                    'name' => $categoria->name,
                ];
            }
        }
        return view('category', [
            'name' => $category->name,
            'parent_id' => $category->parent_id,
            'productos' => $productos,
            'categories' => $categorias
        ]);

    }

    public function showAll()
    {
        $modelo = App\Product::all();
        $productos = [];

        foreach ($modelo as $producto ) {
            $image = '';
            foreach ($producto->images as $img) {
                $image = $img->url;
            }
            $productos[$producto->id] = [
                'name' => $producto->name, 
                'image' => $image,
                'id' => $producto->id,
                'price' => number_format($producto->price, 2)
            ];
        }
        $modelo = App\Category::all();
        $categorias = [];
        foreach ($modelo as $categoria) {
            if (is_null($categoria->parent_id)){
                $categorias[$categoria->id] = [
                    'id' => $categoria->id,
                    'name' => $categoria->name,
                    'products' => $categoria->products()->count(),
                    'categories' => []
                ];
            } else {
                $categorias[$categoria->parent_id]['categories'][] = [
                    'id' => $categoria->id,
                    'name' => $categoria->name,
                ];
            }
        }
        return view('category', [
            'productos' => $productos,
            'categories' => $categorias
        ]);
    }
}
