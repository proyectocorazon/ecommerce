<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;

class Product extends Controller
{
    public function show($id)
    {
        $product = App\Product::find($id);
        if (is_null($product)) {
            return redirect('categoria');
        }
        $images = [];
        $p_image = 0;
        foreach ($product->images as $image) {
            if ($p_image == 0) {
                $p_image = $image->url;
            }
            $images[$image->id] = $image->url;
        }
        $modelo = App\Category::all();
        $categorias = [];
        foreach ($modelo as $categoria) {
            if (is_null($categoria->parent_id)){
                $categorias[$categoria->id] = [
                    'id' => $categoria->id,
                    'name' => $categoria->name,
                    'products' => $categoria->products()->count(),
                    'categories' => []
                ];
            } else {
                $categorias[$categoria->parent_id]['categories'][] = [
                    'id' => $categoria->id,
                    'name' => $categoria->name,
                ];
            }
        }
        $sup_categories = [];
        foreach ($product->categories as $category) {
            if (is_null($category->parent_id)) {
                if (array_key_exists($category->id, $sup_categories)) {
                    $sup_categories[$category->id]['id'] = $category->id;
                    $sup_categories[$category->id]['name'] = $category->name;
                } else {
                    $sup_categories[$category->id] = [
                        'id' => $category->id,
                        'name' => $category->name
                    ];
                }
            } else {
                if (array_key_exists($category->parent_id, $sup_categories)) {
                    $sup_categories[$category->parent_id]['sub_category'][$category->id] = [
                        'id' => $category->id,
                        'name' => $category->name
                    ];
                } else {
                    $sup_categories[$category->parent_id] = [
                        'sub_category' => [
                            $category->id => [
                                'id' => $category->id,
                                'name' => $category->name
                            ]
                        ]
                    ];
                }
            }
        }
        return view('product', [
            'name' => $product->name,
            'description' => $product->description,
            'sku' => $product->sku,
            'price' => number_format($product->price, 2),
            'qty' => $product->qty,
            'height' => $product->height,
            'width' => $product->width,
            'length' => $product->length,
            'p_image' => $p_image,
            'images' => $images,
            'categories' => $categorias,
            'sup_categories' => $sup_categories
        ]);
    }

    public function populate()
    {
        $coats = new App\Category;
        $coats->name = "Coats";
        $coats->save();

        $blouses = new App\Category;
        $blouses->name = "Blouses";
        $blouses->save();

        $b_blouses = new App\Category;
        $b_blouses->parent_id = $blouses->id;
        $b_blouses->name = "Black Blouses";
        $b_blouses->save();

        $w_blouses = new App\Category;
        $w_blouses->parent_id = $blouses->id;
        $w_blouses->name = "White Blouses";
        $w_blouses->save();

        $f_coats = new App\Category;
        $f_coats->parent_id = $coats->id;
        $f_coats->name = "Fur Coats";
        $f_coats->save();

        $product = new App\Product;
        $product->name = "White Bluse Armani";
        $product->description = "Built purse maids cease her ham new seven among and. Pulled coming wooded tended it answer remain me be.";
        $product->sku = "WBA41212";
        $product->price = 2355;
        $product->qty = 4;
        $product->height = 0;
        $product->width = 0;
        $product->length = 0;
        $product->save();
        $product->categories()->save($w_blouses);
        $product->categories()->save($blouses);
        $image = new App\Image;
        $image->product_id = $product->id;
        $image->url = 'product2.jpg';
        $image->save();

        $product = new App\Product;
        $product->name = "Fur Coat With Very But Very Very Long Name";
        $product->description = "Fur Coat With Very But Very Very Long Name Fur Coat With Very But Very Very Long Name";
        $product->sku = "FCWVBVVLN";
        $product->price = 1214;
        $product->qty = 56;
        $product->height = 0;
        $product->width = 0;
        $product->length = 0;
        $product->save();
        $product->categories()->save($f_coats);
        $product->categories()->save($coats);
        $image = new App\Image;
        $image->product_id = $product->id;
        $image->url = 'product1.jpg';
        $image->save();

        $product = new App\Product;
        $product->name = "Black Blouse Versace";
        $product->description = "Black Blouse Versace Black Blouse Versace Black Blouse Versace Black Blouse Versace";
        $product->sku = "BBV45143";
        $product->price = 4813;
        $product->qty = 42;
        $product->height = 0;
        $product->width = 0;
        $product->length = 0;
        $product->save();
        $product->categories()->save($b_blouses);
        $product->categories()->save($blouses);
        $image = new App\Image;
        $image->product_id = $product->id;
        $image->url = 'product3.jpg';
        $image->save();

        $product = new App\Product;
        $product->name = "Something Something Product 1";
        $product->description = "Something Something Product 1 Something Something Product 1 Something Something Product 1";
        $product->sku = "SSP13221";
        $product->price = 382;
        $product->qty = 39;
        $product->height = 0;
        $product->width = 0;
        $product->length = 0;
        $product->save();
        $product->categories()->save($b_blouses);
        $product->categories()->save($blouses);
        $image = new App\Image;
        $image->product_id = $product->id;
        $image->url = 'product3.jpg';
        $image->save();

        $product = new App\Product;
        $product->name = "Something Something Product 2";
        $product->description = "Something Something Product 2 Something Something Product 2 Something Something Product 2";
        $product->sku = "SSP23222";
        $product->price = 488;
        $product->qty = 92;
        $product->height = 0;
        $product->width = 0;
        $product->length = 0;
        $product->save();
        $product->categories()->save($w_blouses);
        $product->categories()->save($blouses);
        $image = new App\Image;
        $image->product_id = $product->id;
        $image->url = 'product2.jpg';
        $image->save();

        $product = new App\Product;
        $product->name = "Something Something Product 3";
        $product->description = "Something Something Product 3 Something Something Product 3 Something Something Product 3";
        $product->sku = "SSP33333";
        $product->price = 881;
        $product->qty = 19;
        $product->height = 0;
        $product->width = 0;
        $product->length = 0;
        $product->save();
        $product->categories()->save($f_coats);
        $product->categories()->save($coats);
        $image = new App\Image;
        $image->product_id = $product->id;
        $image->url = 'product1.jpg';
        $image->save();

        $product = new App\Product;
        $product->name = "Something Something Product 4";
        $product->description = "Something Something Product 4 Something Something Product 4 Something Something Product 4";
        $product->sku = "SSP423137";
        $product->price = 3772;
        $product->qty = 49;
        $product->height = 0;
        $product->width = 0;
        $product->length = 0;
        $product->save();
        $product->categories()->save($w_blouses);
        $product->categories()->save($blouses);
        $image = new App\Image;
        $image->product_id = $product->id;
        $image->url = 'product2.jpg';
        $image->save();

        $product = new App\Product;
        $product->name = "Something Something Product 5";
        $product->description = "Something Something Product 5 Something Something Product 5 Something Something Product 5";
        $product->sku = "SSP523137";
        $product->price = 4881;
        $product->qty = 71;
        $product->height = 0;
        $product->width = 0;
        $product->length = 0;
        $product->save();
        $product->categories()->save($b_blouses);
        $image = new App\Image;
        $image->product_id = $product->id;
        $image->url = 'product3.jpg';
        $image->save();

        $product = new App\Product;
        $product->name = "Something Something Product 6";
        $product->description = "Something Something Product 6 Something Something Product 6 Something Something Product 6";
        $product->sku = "SSP623137";
        $product->price = 9184;
        $product->qty = 381;
        $product->height = 0;
        $product->width = 0;
        $product->length = 0;
        $product->save();
        $product->categories()->save($w_blouses);
        $image = new App\Image;
        $image->product_id = $product->id;
        $image->url = 'product2.jpg';
        $image->save();

        $product = new App\Product;
        $product->name = "Something Something Product 7";
        $product->description = "Something Something Product 7 Something Something Product 7 Something Something Product 7";
        $product->sku = "SSP723137";
        $product->price = 491;
        $product->qty = 0;
        $product->height = 0;
        $product->width = 0;
        $product->length = 0;
        $product->save();
        $product->categories()->save($f_coats);
        $image = new App\Image;
        $image->product_id = $product->id;
        $image->url = 'product1.jpg';
        $image->save();
        $image = new App\Image;
        $image->product_id = $product->id;
        $image->url = 'product2.jpg';
        $image->save();

        return '';
    }
}
