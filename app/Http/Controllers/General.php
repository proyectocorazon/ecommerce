<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
class General extends Controller
{
    public function showIndex()
    {
        $modelo = App\Product::all();
        $productos = [];
        $contador = 0;
        foreach ($modelo as $producto) {
            $image = '';
            foreach ($producto->images as $img) {
                $image = $img->url;
            }
            $productos[$producto->id] = [
                'name' => $producto->name, 
                'image' => $image,
                'id' => $producto->id
            ];
            $contador++;
            if ($contador >= 12)
                break;
        }
    	return view('index', ['productos' => $productos]);
    }

    public function showUs()
    {
    	return view('us');
    }

    public function showTutorials()
    {
    	return view('tuto');
    }
}
