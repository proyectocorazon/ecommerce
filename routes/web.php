<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'General@showIndex');
Route::get('inicio', 'General@showIndex');
Route::get('nosotros', 'General@showUs');
Route::get('tutorial', 'General@showTutorials');
Route::get('contacto', 'Contact@show')->name('contact');


Route::group(['prefix' => 'categoria'], function() {
    Route::get('/', 'Category@showAll');
    Route::get('{id}', 'Category@show');
});

Route::group(['prefix' => 'producto'], function () {
    Route::get('/', function() {
        return redirect('categoria');
    });

    Route::get('{id}', 'Product@show');
});

Route::get('testing', 'Product@populate');

Route::post('send/message', 'Contact@sendMessage');

