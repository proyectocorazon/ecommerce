@extends('layouts.base')
@section('content')


        <div id="heading-breadcrumbs" style="margin-top: 67px;">
            <div class="container">
                <div class="row">
                    <div class="col-md-7">
                        <h1>{{ $name or "Categorias"}}</h1>
                    </div>
                    <div class="col-md-5">
                        <ul class="breadcrumb">
                            <li><a href="{{ asset('/') }}">Inicio</a>
                            </li>
                            <li>{{ $name or "Categorias"}}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div id="content">
            <div class="container">

                <div class="row">


                    <!-- *** LEFT COLUMN ***
            _________________________________________________________ -->

                    <div class="col-sm-3">

                        <!-- *** MENUS AND FILTERS ***
 _________________________________________________________ -->
                        <div class="panel panel-default sidebar-menu">

                            <div class="panel-heading">
                                <h3 class="panel-title">Categorias</h3>
                            </div>

                            <div class="panel-body">
                                <ul class="nav nav-pills nav-stacked category-menu">
                                    @foreach ($categories as $categoria)
                                    <li>
                                        <a href="{{ asset('categoria/'.$categoria['id']) }}">{{ $categoria['name'] }} <span class="badge pull-right">{{ $categoria['products'] }}</span></a>
                                        <ul>
                                        @foreach ($categoria['categories'] as $subcategoria)
                                            <li><a href="{{ asset('categoria/'.$subcategoria['id']) }}">{{ $subcategoria['name'] }}</a>
                                            </li>
                                        @endforeach
                                        </ul>
                                    </li>
                                    @endforeach

                                </ul>

                            </div>
                        </div>
                    </div>
                    <!-- /.col-md-3 -->

                    <!-- *** LEFT COLUMN END *** -->

                    <!-- *** RIGHT COLUMN ***
            _________________________________________________________ -->

                    <div class="col-sm-9">

                        <div class="row products">

                            @foreach ($productos as $product)
                            <div class="col-md-4 col-sm-6">
                                <div class="product">
                                    <div class="image">
                                        <a href="{{ asset('producto/'.$product['id']) }}">
                                            <img src="{{ asset('img/'.$product['image']) }}" alt="" class="img-responsive image1">
                                        </a>
                                    </div>
                                    <!-- /.image -->
                                    <div class="text">
                                        <h3><a href="{{ asset('producto/'.$product['id']) }}">{{ $product['name'] }}</a></h3>
                                        <p class="price">${{ $product['price'] }}</p>
                                        <p class="buttons">
                                            <a href="shop-detail.html" class="btn btn-default">View detail</a>
                                            <a href="shop-basket.html" class="btn btn-template-main"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                        </p>
                                    </div>
                                    <!-- /.text -->
                                </div>
                                <!-- /.product -->
                            </div>
                            @endforeach
                            
                        </div>
                        <!-- /.products -->

                       <!--
                        <div class="pages">

                            <p class="loadMore">
                                <a href="#" class="btn btn-template-main"><i class="fa fa-chevron-down"></i> Load more</a>
                            </p>

                            <ul class="pagination">
                                <li><a href="#">&laquo;</a>
                                </li>
                                <li class="active"><a href="#">1</a>
                                </li>
                                <li><a href="#">2</a>
                                </li>
                                <li><a href="#">3</a>
                                </li>
                                <li><a href="#">4</a>
                                </li>
                                <li><a href="#">5</a>
                                </li>
                                <li><a href="#">&raquo;</a>
                                </li>
                            </ul>
                        </div>
                        -->

                    </div>
                    <!-- /.col-md-9 -->

                    <!-- *** RIGHT COLUMN END *** -->

                </div>

            </div>
            <!-- /.container -->
        </div>
        <!-- /#content -->

@endsection
