@extends('layouts.base')
@section('content')

    <div id="heading-breadcrumbs" style="margin-top: 67px;">
            <div class="container">
                <div class="row">
                    <div class="col-md-7">
                        <h1>{{ $name or "Ha ocurrido un error" }}</h1>
                    </div>
                    <div class="col-md-5">
                        <ul class="breadcrumb">
                            <li><a href="{{ asset('/') }}">Inicio</a>
                            </li>
                            @foreach ($sup_categories as $category)
                                @if (isset($category['id']))
                                    <li><a href="{{ asset('categoria/' . $category['id']) }}">{{ $category['name'] }}</a>
                                    </li>
                                @endif
                                @foreach ($category['sub_category'] as $subcategoria)
                                    <li><a href="{{ asset('categoria/' . $subcategoria['id']) }}">{{ $subcategoria['name'] }}</a>
                                    </li>
                                @endforeach
                            @endforeach
                            <li>{{ $name or "Ha ocurrido un error" }}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div id="content">
            <div class="container">

                <div class="row">

                    <!-- *** LEFT COLUMN ***
            _________________________________________________________ -->

                    <div class="col-md-9">

                        <div class="row" id="productMain">
                            <div class="col-sm-6">
                                <div id="mainImage">
                                    <img style="height: 460px; width: 500px; background-size: auto 100% !important; background-position: 50% !important; background-repeat: no-repeat !important;" src="{{ asset('img/' . $p_image) }}" alt="" class="img-responsive">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="box">
                                     <p class="lead">{{ $description }}</p>
                                     <form>
                                        <div class="sizes">

                                            <h3 style="margin-bottom: 15px !important">Tamaños</h3>

                                            <label for="size_s">
                                                <a href="#">S</a>
                                                <input type="radio" id="size_s" name="size" value="s" class="size-input">
                                            </label>
                                            <label for="size_m">
                                                <a href="#">M</a>
                                                <input type="radio" id="size_m" name="size" value="m" class="size-input">
                                            </label>
                                            <label for="size_l">
                                                <a href="#">L</a>
                                                <input type="radio" id="size_l" name="size" value="l" class="size-input">
                                            </label>

                                        </div>

                                        <p class="price" style="font-size: 26px !important">Precio: ${{ $price }}</p>

                                        

                                    </form>
                                
                                </div>

                                <div class="row" id="thumbs">
                                    @foreach ($images as $image)
                                    <div class="col-xs-4">
                                        <a href="{{ asset('img/' . $image) }}" class="thumb">
                                            <img src="{{ asset('img/' . $image) }}" alt="" class="img-responsive">
                                        </a>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.col-md-9 -->


                    <!-- *** LEFT COLUMN END *** -->

                    <!-- *** RIGHT COLUMN ***
          _________________________________________________________ -->

                    <div class="col-sm-3">

                        <!-- *** MENUS AND FILTERS ***
 _________________________________________________________ -->
                        <div class="panel panel-default sidebar-menu">

                            <div class="panel-heading">
                                <h3 class="panel-title">Categorias</h3>
                            </div>

                            <div class="panel-body">
                                <ul class="nav nav-pills nav-stacked category-menu">
                                
                                    @foreach ($categories as $categoria)
                                    <li>
                                        <a href="{{ asset('categoria/'.$categoria['id']) }}">{{ $categoria['name'] }} <span class="badge pull-right">{{ $categoria['products'] }}</span></a>
                                        <ul>
                                        @foreach ($categoria['categories'] as $subcategoria)
                                            <li><a href="{{ asset('categoria/'.$subcategoria['id']) }}">{{ $subcategoria['name'] }}</a>
                                            </li>
                                        @endforeach
                                        </ul>
                                    </li>
                                    @endforeach

                                </ul>

                            </div>
                        </div>

                        
                        <!-- *** MENUS AND FILTERS END *** -->


                    <!-- *** RIGHT COLUMN END *** -->

                </div>
                <!-- /.row -->

            </div>
            <!-- /.container -->
        </div>
        <!-- /#content -->

 @endsection
