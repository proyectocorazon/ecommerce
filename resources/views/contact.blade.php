@extends('layouts.base')
@section('content')
    <div id="heading-breadcrumbs" class="no-mb" style="margin-top:67px;">
            <div class="container">
                <div class="row">
                    <div class="col-md-7">
                        <h1>Contacto</h1>
                    </div>
                    <div class="col-md-5">
                        <ul class="breadcrumb">
                            <li><a href="{{ asset('/') }}">Inicio</a>
                            </li>
                            <li>Contacto</li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>

        <section>
            <div id="map" class="with-border">
            </div>
        </section>

        <div id="content">
            <div class="container" id="contact">

                <section>
                    <div class="row">
                        <div class="col-md-8">

                            <div class="heading">
                                <h3>Contacto</h3>
                            </div>

                            <form action="{{ asset('/send/message') }}" method="POST">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="firstname">Nombre</label>
                                            <input type="text" class="form-control" id="firstname" name="firstname" value="{{ old('firstname') }}">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="lastname">Apellidos</label>
                                            <input type="text" class="form-control" id="lastname" name="lastname" value="{{ old('lastname') }}">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="email">Email</label>
                                            <input type="text" class="form-control" id="email" name="email" value="{{ old('email') }}">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="subject">Telefono</label>
                                            <input type="text" class="form-control" id="phone" name="phone" value="{{ old('phone') }}">
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="message">Mensaje</label>
                                            <textarea id="message" class="form-control" name="message" value="{{ old('message') }}"></textarea>
                                        </div>
                                    </div>

                                    <div class="col-sm-12 text-center">
                                        <button type="submit" class="btn btn-template-main"><i class="fa fa-envelope-o"></i> Enviar mensaje</button>

                                    </div>
                                </div>
                                <!-- /.row -->
                            </form>
                        </div>

                        <div class="col-md-4">

                            <h3 class="text-uppercase">Direcci&oacute;n</h3>

                            <p>13/25 New Avenue
                                <br>New Heaven
                                <br>45Y 73J
                                <br>England
                                <br>
                                <strong>Great Britain</strong>
                            </p>

                            <h3 class="text-uppercase">Electronic support</h3>

                            <p class="text-muted">Please feel free to write an email to us or to use our electronic ticketing system.</p>
                            <ul>
                                <li><strong><a href="mailto:">info@fakeemail.com</a></strong>
                                </li>
                                <li><strong><a href="#">Ticketio</a></strong> - our ticketing support platform</li>
                            </ul>


                        </div>

                    </div>


                </section>

            </div>
            <!-- /#contact.container -->
        </div>


 @endsection
