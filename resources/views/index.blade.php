@extends('layouts.base')
@section('content')
        <section style="margin-bottom: 0px !important;">
            <!-- *** HOMEPAGE***
 _________________________________________________________ -->

            <div class="home-carousel" style="margin-top: 67px;">

                <div class="dark-mask"></div>

                <div class="container">
                    <div class="homepage owl-carousel">
                        <div class="item">
                            <div class="row">
                                @foreach ($productos as $producto)
                                <div class="col-md-2 col-sm-2">
                                    <div class="team-member" data-animate="fadeInUp">
                                        <div class="image">
                                            <a href="{{ asset('producto/'.$producto['id']) }}">
                                                <img src="{{ asset('img/'.$producto['image']) }}" alt="" class="img-responsive img-circle">
                                            </a>
                                        </div>
                                        <h3><a href="{{ asset('producto/'.$producto['id']) }}">{{ $producto['name'] }}</a></h3>
                                        <!-- <p class="role">Founder</p> -->
                                    </div>
                                    <!-- /.team-member -->
                                </div>
                                @endforeach
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- *** HOMEPAGE CAROUSEL END *** -->
        </section>

        <section class="bar background-gray no-mb padding-big text-center-sm">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h2 class="text-uppercase">Everything you need</h2>
                        <p class="lead mb-small">40+ prepared HTML templates</p>
                        <p class="mb-small">We have prepared literally <strong>everything you would possibly need building corporate, e-commerce or portfolio website</strong>. If you still miss something, let us know and we will try to include it in theme's future updates.</p>

                        <p><a href="#" class="btn btn-template-main">Read more</a>
                        </p>
                    </div>
                    <div class="col-md-6 text-center">
                        <img src="{{ asset('img/template-easy-customize.png') }}" alt="" class="img-responsive">
                    </div>
                </div>
            </div>
        </section>

        <section class="bar background-image-fixed-2 no-mb color-white text-center">
            <div class="dark-mask"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="icon icon-lg"><i class="fa fa-file-code-o"></i>
                        </div>
                        <h1>Do you want to see more?</h1>
                        <p class="lead">We have prepared for you more than 40 different HTML pages, including 5 variations of homepage.</p>
                        <p class="text-center">
                            <a href="{{ asset('/') }}" class="btn btn-template-transparent-black btn-lg">Check other homepage</a>
                        </p>
                    </div>

                </div>
            </div>
        </section>

        <section class="bar background-gray no-mb">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="owl-carousel customers no-mb">
                            <li class="item">
                                <img src="{{ asset('img/customer-1.png') }}" alt="" class="img-responsive">
                            </li>
                            <li class="item">
                                <img src="{{ asset('img/customer-2.png') }}" alt="" class="img-responsive">
                            </li>
                            <li class="item">
                                <img src="{{ asset('img/customer-3.png') }}" alt="" class="img-responsive">
                            </li>
                            <li class="item">
                                <img src="{{ asset('img/customer-4.png') }}" alt="" class="img-responsive">
                            </li>
                            <li class="item">
                                <img src="{{ asset('img/customer-5.png') }}" alt="" class="img-responsive">
                            </li>
                            <li class="item">
                                <img src="{{ asset('img/customer-6.png') }}" alt="" class="img-responsive">
                            </li>
                        </ul>
                        <!-- /.owl-carousel -->
                    </div>

                </div>
            </div>
        </section>

       
        <!-- /.bar -->

 @endsection
    
